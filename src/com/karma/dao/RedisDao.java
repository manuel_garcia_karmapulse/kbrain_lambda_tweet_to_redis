package com.karma.dao;

import java.util.Map;

import redis.clients.jedis.Jedis;

/**
 * Karma pulse
 * @author Manuel
 *
 */
public class RedisDao {
	Jedis jedis;
	

	 public void connect() {
	      //Connecting to Redis server
	      jedis = new Jedis("ec2-54-191-90-247.us-west-2.compute.amazonaws.com",6379);	      
	      System.out.println("Connection to server sucessfully: ");//+jedis.info());
	      //check whether server is running or not
	      System.out.println("Server is running: "+jedis.ping()+ " : "+jedis.isConnected()); 
	      	      
	 }
	 
	 /**
	  * 
	  * @return Redis connection
	  */
	 public Jedis getConnection(){
		 return jedis;
	 }  
	
	 public void disconnect(){
		 jedis.disconnect();
		 System.out.println("Server running: "+jedis.isConnected());
	 }
	
}
