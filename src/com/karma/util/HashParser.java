package com.karma.util;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.karma.dao.RedisDao;

/**
 * KarmaPulse
 * @author Manuel
 *
 */
public class HashParser {
	String parentKey = null;
	String idSearch = null;
	String idTweet = null;
	RedisDao dao = null; 
	final Double INCREMENT = 1d; 
	final String IMAGE_TAG = "images";
	final String GIF_TAG = "gifs";
	final String VIDEO_TAG = "videos";	
	
	public HashParser(){
		dao = new RedisDao();
	}
	
	
	/**
	 * 
	 * @param tweetJSON
	 * @return trend Map ready to store in Redis
	 * @throws JSONException
	 */
	public void parseTweet(JSONObject tweetJSON) throws JSONException{
		dao.connect();
	    final int ID_TAG_POS = 0; 	
		JSONObject gnip = tweetJSON.getJSONObject("gnip");
		JSONArray matching_rules = gnip.getJSONArray("matching_rules");
		JSONObject matching_rule = matching_rules.getJSONObject(ID_TAG_POS);
		String tag =  (matching_rule.isNull("tag")?"default":matching_rule.getString("tag"));
		idSearch = parseSearchId(tag);
		idTweet = this.parseTweetId(tweetJSON.getString("id"));
		parentKey = "trend:tw:"+idSearch+":";
		rankTweet(tweetJSON);
		hashTweet(tweetJSON);
				
		dao.disconnect();
	}
	
	
	void hashTweet(JSONObject tweetJSON) throws JSONException{
		
		JSONObject actor = tweetJSON.getJSONObject("actor");
		String preferredUsername = actor.getString("preferredUsername");
		
		String userKey = parentKey+"users:"+preferredUsername;
		
		//USER
		Map<String, String> userProperties = new HashMap<String, String>();				
		userProperties.put("username",preferredUsername);		
		userProperties.put("user_image",actor.getString("image"));
		userProperties.put("name",actor.getString("displayName"));
		userProperties.put("verified",Boolean.toString(actor.getBoolean("verified")));						
		dao.getConnection().hmset(userKey, userProperties);
		
		//URL
		JSONObject twitter_entities = tweetJSON.getJSONObject("twitter_entities");
		JSONArray urls = twitter_entities.getJSONArray("urls");
		
		for (int index = 0 ; index < urls.length() ; index++) {
			JSONObject urlJSON = urls.getJSONObject(index);
			String expandedUrl = urlJSON.getString("expanded_url");
			String urlsKey = parentKey+"urls:"+expandedUrl;
			Map<String, String> urlProperties = new HashMap<String, String>();
			urlProperties.put("url", expandedUrl); 			
			//GNIP
			
			JSONObject gnip = tweetJSON.getJSONObject("gnip");
			JSONArray gnip_urls = gnip.getJSONArray("urls");
			//System.out.println("urls:::  "+ gnip_urls.length() );
			if( gnip_urls != null && gnip_urls.length()>0 ){
				
				for( int index_gnip = 0 ; index_gnip < gnip_urls.length() ; index_gnip++ ){
					JSONObject gnip_url = gnip_urls.getJSONObject(index_gnip);
					//System.out.println("urls compare :::  "+urlJSON.getString("url")+" : "+ gnip_url.getString("url") );
					if( urlJSON.getString("url").equals( gnip_url.getString("url") ) ){
						System.out.println("urls finded :::  "+ gnip_url.getString("url") );
						String title = gnip_url.getString("expanded_url_title");
						String text = gnip_url.getString("expanded_url_description");
						urlProperties.put("url_image", "not available yet");
						urlProperties.put("title", title);
						urlProperties.put("text", text);						
					}
				}
				
			}
			
			dao.getConnection().hmset(urlsKey, urlProperties);			
		}
						
	}
	
		
	/**
	 * 
	 * @param tweetJSON
	 * @throws JSONException
	 */
	void rankTweet(JSONObject tweetJSON) throws JSONException{
				
		//tweet type
		JSONObject actor = tweetJSON.getJSONObject("actor");
	    String verb = tweetJSON.getString("verb");
	    verb = (verb.equals("share")?"retweet":verb);
	    verb = (verb.equals("post")?"original":verb);
	    verb = (tweetJSON.has("twitter_quoted_status")?"quoted":verb);		
		dao.getConnection().incr(parentKey+"tweet_type:"+verb);
	    	   		
		//App
		JSONObject generator =  tweetJSON.getJSONObject("generator");
		String app = generator.getString("displayName");
		dao.getConnection().zincrby(parentKey+"applications", INCREMENT, app);
	    
		//User appearance
		String preferredUsername = actor.getString("preferredUsername");
		dao.getConnection().zincrby(parentKey+"user_appearances", INCREMENT, preferredUsername);
		
		// location
		JSONObject location = actor.getJSONObject("location");
		String place_name = location.getString("displayName");
		place_name = (place_name == null || place_name.equals("")?"SIN UBICACION":place_name);
		dao.getConnection().zincrby(parentKey+"locations", INCREMENT, place_name);
		
		//Followers
		Integer followersCount = actor.getInt("followersCount");
		dao.getConnection().zadd(parentKey+"user_followers", followersCount, preferredUsername);
		
		//hashtags
		JSONObject twitter_entities = tweetJSON.getJSONObject("twitter_entities");		        
		JSONArray hashtags = twitter_entities.getJSONArray("hashtags");		
		putArray(hashtags, parentKey+"hashtags", "text");
		
		// User mentions
		JSONArray user_mentions = twitter_entities.getJSONArray("user_mentions");		
		putArray(user_mentions, parentKey+"mentions", "screen_name");
		
		//Urls
		JSONArray urls = twitter_entities.getJSONArray("urls");
		putArray(urls, parentKey+"urls", "expanded_url");
		
		// images, video , gif
		if (tweetJSON.has("twitter_extended_entities")) {			
			JSONObject twitter_extended_entities = tweetJSON
					.getJSONObject("twitter_extended_entities");
			JSONArray media = twitter_extended_entities.getJSONArray("media");

			for (int index = 0; index < media.length(); index++) {
				JSONObject mediaObject = media.getJSONObject(index);
				String type_name = mediaObject.getString("type");
				if (type_name.equals("photo")) {
					type_name = IMAGE_TAG;
				} else
				if (type_name.equals("animated_gif")) {
					type_name = GIF_TAG;
				} else
				if (type_name.equals("video")) {
					type_name = VIDEO_TAG;
				}
				dao.getConnection().zincrby(parentKey + type_name, INCREMENT,
						mediaObject.getString("media_url"));
				hashMedia(mediaObject, actor, tweetJSON, type_name);
			}
		}
		
				
		// sentiment score
		if (tweetJSON.has("classifications")) {
			JSONArray classifications = tweetJSON.getJSONArray("classifications");

			for (int index = 0; index < classifications.length(); index++) {
				String clasificacion = classifications.getString(index);

				switch (clasificacion) {
				case "positivo":
					dao.getConnection().incr(parentKey + "sentiment:positives");
					dao.getConnection().zincrby(parentKey+"audience_positive", INCREMENT, preferredUsername);
					break;
				case "negativo":
					dao.getConnection().incr(parentKey + "sentiment:negatives");
					dao.getConnection().zincrby(parentKey+"audience_negative", INCREMENT, preferredUsername);
					break;
				case "polarizado":
					dao.getConnection().incr(parentKey + "sentiment:polarized");
					//dao.getConnection().zincrby(parentKey+"audience_polarized", INCREMENT, preferredUsername);
					break;
				case "neutral":
					dao.getConnection().incr(parentKey + "sentiment:neutrals");
					//dao.getConnection().zincrby(parentKey+"audience_neutral", INCREMENT, preferredUsername);
					break;
				default:
					System.out.println("UNDEFINED SENTIMENT: " + clasificacion);
					dao.getConnection().incr(parentKey + "sentiment:" + clasificacion);
					dao.getConnection().zincrby(parentKey+"audience_"+clasificacion, INCREMENT, preferredUsername);
					break;
				}
			}
		}		
	}
	
	
	
	/**
	 * 
	 * @param mediaObject
	 * @param type_name
	 * @throws JSONException 
	 */
	void hashMedia(JSONObject mediaObject, JSONObject actor, JSONObject tweetJSON, String type_name)
			throws JSONException {

		String media_url = mediaObject.getString("media_url");
		String mediaKey = parentKey+type_name+":" + media_url;
		JSONObject video_info = null; 

		Map<String, String> mediaProperties = new HashMap<String, String>();
		
		switch (type_name) {
		case IMAGE_TAG:
			mediaProperties.put("media_url", media_url);
			mediaProperties.put("tweet_id", idTweet);
			mediaProperties.put("tweet_url", tweetJSON.getString("link"));
			mediaProperties.put("tweet_posted_time", tweetJSON.getString("postedTime"));
			mediaProperties.put("tweet_body", tweetJSON.getString("body"));
			mediaProperties.put("user_image", actor.getString("image"));
			mediaProperties.put("username",actor.getString("preferredUsername"));
			mediaProperties.put("name", actor.getString("displayName"));
			break;			
		case VIDEO_TAG:
			video_info = mediaObject.getJSONObject("video_info");
			mediaProperties.put("media_url", media_url);			
			mediaProperties.put("media_duration_millis", video_info.getString("duration_millis"));
			mediaProperties.put("media_aspect_ratio", video_info.getJSONArray("aspect_ratio").toString());
			mediaProperties.put("media_variants", video_info.getJSONArray("variants").toString());
			mediaProperties.put("tweet_id", idTweet);
			mediaProperties.put("tweet_url", tweetJSON.getString("link"));
			mediaProperties.put("tweet_posted_time", tweetJSON.getString("postedTime"));
			mediaProperties.put("tweet_body", tweetJSON.getString("body"));
			mediaProperties.put("user_image", actor.getString("image"));
			mediaProperties.put("username", actor.getString("preferredUsername"));
			mediaProperties.put("name", actor.getString("displayName"));			
			break;			
		case GIF_TAG:
			video_info = mediaObject.getJSONObject("video_info");
			mediaProperties.put("media_url", media_url);			
			mediaProperties.put("media_aspect_ratio", video_info.getJSONArray("aspect_ratio").toString());
			mediaProperties.put("media_variants", video_info.getJSONArray("variants").toString());
			mediaProperties.put("tweet_id", idTweet);
			mediaProperties.put("tweet_url", tweetJSON.getString("link"));
			mediaProperties.put("tweet_posted_time", tweetJSON.getString("postedTime"));
			mediaProperties.put("tweet_body", tweetJSON.getString("body"));
			mediaProperties.put("user_image", actor.getString("image"));
			mediaProperties.put("username", actor.getString("preferredUsername"));
			mediaProperties.put("name", actor.getString("displayName"));
			break;			
		 default: System.out.println ("TYPE NOT FOUND: "+type_name); 
		 	break;
		}

		dao.getConnection().hmset(mediaKey, mediaProperties);

	}
	
	
	/**
	 * 
	 * @param jsonArray
	 * @param key
	 * @param attribute
	 * @throws JSONException
	 */
	void putArray(JSONArray jsonArray , String key, String attribute) throws JSONException{
		for (int index = 0 ; index < jsonArray.length() ; index++) {
			JSONObject jsonObject = jsonArray.getJSONObject(index);
			dao.getConnection().zincrby(key, INCREMENT, jsonObject.getString(attribute));
        }
	}
	
			
	String parseSearchId(String idGnip){
		final int ID_INDEX = 0; 
		String[] parts = idGnip.split("-");
		return parts[ID_INDEX];
	}
	
	String parseTweetId(String idGnip){
		final int ID_INDEX = 2; 
		String[] parts = idGnip.split(":");
		return parts[ID_INDEX];
	}

}
