package com.karma;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.KinesisEvent;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.karma.util.HashParser;
/**
 * KarmaPulse
 * @author Manuel
 *
 */
public class LambdaFunctionHandler implements RequestHandler<KinesisEvent, Object> {
	
	HashParser hashparser = null;
	
	public LambdaFunctionHandler(){
		hashparser = new HashParser();
	}

    @Override
    public Object handleRequest(KinesisEvent input, Context context) {
        context.getLogger().log("Input: " + input);
        String response = "None";
    	        
        for(KinesisEvent.KinesisEventRecord record : input.getRecords()){
        	String received = new String(record.getKinesis().getData().array());
        	JSONObject tweetJSON = null;
        	System.out.println("RECORD: "+ received);
        	
        	      	
        	try {
				tweetJSON = new JSONObject(received);							
			} catch (JSONException e) {
				context.getLogger().log("Invalid or Unexpected Kinesis Input. Exception: " +e.getMessage());
				e.printStackTrace();
			}
        	
        	if(tweetJSON != null){
        		//parse and save
        		try {
        			hashparser.parseTweet(tweetJSON);
				} catch (JSONException e) {
					context.getLogger().log("Exception parsing JSON: " +e.getMessage());
					e.printStackTrace();
				}
        	}
        	
        }
                        
        return response;
    }

}
